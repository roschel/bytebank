FROM python:3.6
# Copiando os arquivos do projeto para o diretorio usr/src/app
COPY . usr/src/app
# Definindo o diretorio onde o CMD será executado e copiando o arquivo de requeriments
WORKDIR /usr/src/app
COPY requirements.txt ./
# Intalando os requerimentos com o PIP
RUN pip install --no-cache-dir -r requirements.txt
# Expondo a porta do APP
EXPOSE 8000
# Executando o comando para subir a aplicação
CMD ["gunicorn", "to_do.wsgi:application", "--bind", "0.0.0.0:8000", "--workers", "3"]